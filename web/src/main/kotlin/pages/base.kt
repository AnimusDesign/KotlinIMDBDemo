package design.animus.imdbdemo.web.pages

import org.w3c.dom.events.Event
import react.*
import react.dom.br
import react.dom.div
import react.materialui.*
import react.router.dom.routeLink


interface PageBaseState : RState {
    var drawerOpen: Boolean
    var namesExpanded: Boolean
    var titlesExpanded: Boolean
}

abstract class BaseComponentWrapper<P : RProps, S : PageBaseState> : RComponent<P, S>() {
    fun handleDrawerChange(event: Event) {
        setState { drawerOpen = !state.drawerOpen }
    }

    fun initState() {

        setState {
            drawerOpen = false
            namesExpanded = false
            titlesExpanded = false
        }
    }

    fun RBuilder.drawerItems() {
        MenuList {
            MenuItem {
                routeLink(to = "/") {
                    Icon { +"home" }
                    +"Home"
                }
            }
            Divider {}
            ExpansionPanel {
                attrs.expanded = state.namesExpanded
                attrs.onChange = { setState { namesExpanded = !namesExpanded } }
                ExpansionPanelSummary {
                    attrs.expandIcon = Icon { +"arrow_drop_down" }
                    Typography { +"Tabular Data" }
                }
                ExpansionPanelDetails {
                    MenuItem {
                        routeLink(to = "/questions/") {
                            Icon { +"help" }
                            +"Questions"

                        }
                    }
                    MenuItem {
                        routeLink(to = "/goals/") {
                            Icon { +"bar_chart" }
                            +"Goals"
                        }
                    }
                    MenuItem {
                        routeLink(to = "/templates/") {
                            Icon { +"notes" }
                            +"Templates"
                        }
                    }
                }
            }
            ExpansionPanel {
                attrs.expanded = state.titlesExpanded
                attrs.onChange = { setState { titlesExpanded = !titlesExpanded } }
                ExpansionPanelSummary {
                    attrs.expandIcon = Icon { +"arrow_drop_down" }
                    Typography { +"Names" }
                }
                ExpansionPanelDetails {
                    MenuItem {
                        routeLink(to = "/names/") {
                            Icon { +"account" }
                            +"Names List"
                        }
                    }
                }
            }
        }
    }

    fun RBuilder.additionalDrawerItems() {
        div {}
    }
}

class PageBase : BaseComponentWrapper<RProps, PageBaseState>() {

    override fun RBuilder.render() {
        val drawer = Drawer {
            attrs.variant = "persistent"
            attrs.open = state.drawerOpen
            attrs.anchor = "left"
            IconButton {
                attrs.onClick = { event -> handleDrawerChange(event) }
                Icon { +"chevron_left" }
            }
            drawerItems()
        }
        div {
            AppBar {
                Toolbar {
                    attrs.disableGutters = !state.drawerOpen
                    IconButton {
                        attrs.onClick = { event -> handleDrawerChange(event) }
                        Icon { +"menu" }
                    }
                    Typography {
                        attrs.variant = "title"
                        +"IMDB Demo App"
                    }
                }
            }
            (1..4).map { br {} }
            Grid {
                attrs {
                    container = true
                    alignContent = "center"
                    alignItems = "center"
                    justify = "center"
                    spacing = 16
                }
                props.children()
            }
        }
    }
}

fun RBuilder.PageBase(children: RBuilder.() -> Unit) {
    child(PageBase::class) {
        children()
    }
}