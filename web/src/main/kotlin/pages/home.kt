package design.animus.imdbdemo.web.pages


import design.animus.imdbdemo.web.components.tabular.NamesTable
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.br
import react.materialui.Card
import react.materialui.CardHeader
import react.materialui.Grid


class Home : RComponent<RProps, RState>() {


    override fun RBuilder.render() {
        PageBase {
            Grid {
                attrs.container = true
                attrs.justify = "center"
                Grid {
                    attrs.item = true
                    attrs.md = 8
                    Card {
                        CardHeader {
                            attrs.title = "IMDB Demp App"
                        }
                    }
                }
                Grid {
                    attrs.item = true
                    attrs.md = 4
                    Card {
                        CardHeader {
                            attrs.title = "IMDB Demp App"
                        }
                    }
                }
                (1..8).map { br { } }
                Grid {
                    attrs.item = true
                    attrs.md = 10
                    NamesTable()
                }
            }
        }
    }
}


fun RBuilder.Home() = child(Home::class) {

}
