package design.animus.imdbdemo.web

import design.animus.imdbdemo.web.pages.Home
import react.RProps
import react.dom.render
import react.router.dom.hashRouter
import react.router.dom.route
import react.router.dom.switch
import kotlin.browser.document
import kotlin.browser.window

interface IdProps : RProps {
    var id: String
}


fun main(args: Array<String>) {
    window.onload = {
        render(document.getElementById("root")!!) {
            hashRouter {
                switch {
                    route("/", Home::class, exact = true)
                    /*
                    route<IdProps>("/template/:id") { props ->
                        console.log("In render template route.")
                        RenderTemplatePage(props.match.params.id)
                    }
                    */
                }
            }
        }
    }
}