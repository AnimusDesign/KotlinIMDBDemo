package design.animus.imdbdemo.web


import design.animus.common.records.imdb.name.NameBasicResponse
import design.animus.js.xmlhttprequest.getBase


suspend fun fetchNames(page: Int = 1, url: String = "http://localhost/api/v1/name") = getBase(
        "$url/$page",
        NameBasicResponse::class,
        NameBasicResponse.serializer()
)