package design.animus.imdbdemo.web

import org.w3c.dom.events.Event

typealias EventHandlerFunction = (Event) -> Unit
typealias EventHandlerWithStringFunction = (Event, String) -> Unit