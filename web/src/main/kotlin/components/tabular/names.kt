package design.animus.imdbdemo.web.components.tabular

import design.animus.common.records.imdb.name.NameBasicResponse
import design.animus.imdbdemo.web.fetchNames
import kotlinx.coroutines.experimental.launch
import react.*
import react.dom.li
import react.dom.ul
import react.materialui.*

interface NamesTableState : RState {
    var dataLoaded: Boolean
    var page: Int
    var names: NameBasicResponse?

}

class NamesTable : RComponent<RProps, NamesTableState>() {
    private fun loadPage(inPage: Int = 1) {
        launch {
            console.log("Making request to names")
            val rspNames = fetchNames(page = inPage)
            setState {
                dataLoaded = true
                page = inPage
                names = rspNames
            }
        }
    }

    override fun componentWillMount() {
        setState {
            dataLoaded = false
            page = 1
        }
        loadPage(state.page)
    }

    private fun nextPage() {
        setState {
            dataLoaded = false
        }
        loadPage(state.page + 1)
    }

    override fun RBuilder.render() {
        when (state.dataLoaded) {
            false -> Typography {
                +"Loading...."
            }
            true -> renderNamesTable()
        }
    }

    private fun RBuilder.renderNamesTable() {
        Card {
            CardHeader {
                attrs.title = "Names Data"
                attrs.subheader = "Page ${state.page}"
            }
            CardContent {
                Table {
                    TableHead {
                        TableRow {
                            TableCell { +"ID" }
                            TableCell { +"Primary Name" }
                            TableCell { +"Primary Profession" }
                            TableCell { +"Featured Titles" }
                            TableCell { +"Birth Year" }
                            TableCell { +"Death Year" }
                        }
                    }
                    TableBody {
                        state.names?.data?.map {
                            TableRow {
                                TableCell { +it.nconst }
                                TableCell { +it.primaryName }
                                TableCell {
                                    ul {
                                        it.primaryProfession.map { li { +it } }
                                    }
                                }
                                TableCell {
                                    ul {
                                        it.knownForTitles.map { li { +it.titleID } }
                                    }
                                }
                                TableCell {
                                    +it.birthYear
                                }
                                TableCell {
                                    it.deathYear
                                }
                            }
                        }
                    }
                }
            }
            Divider { attrs.inset = true }
            Button {
                attrs.onClick = {
                    nextPage()
                }
                +"Next Page"
            }
        }
    }
}

fun RBuilder.NamesTable() = child(NamesTable::class) {
}
