**Bug**

There is an active bug where the `common/js` package
is not copied properly to the build directory. In short
the requisite data classes for the frontend don't get bundled.
The hack I came up with was to symlink them, like so. 

```bash
 ln -s `pwd`/common/javascript/build/classes/kotlin/main/common_javascript* `pwd`/web/build/js
```

# Summary

This is a react base view of the data set from IMDB. 
It is meant to be a very simple example of pulling data.
A simple table is provided on the main page. The code is 
under the components folder.

This uses REST endpoints, as I have not ported the GraphQL
libraries as of yet.

Two external libraries are utilized. 

* [React Material Bindings](https://gitlab.com/AnimusDesign/KotlinReactMaterialUI) - Ported type script definitions to Kotlin.
* [XMLHTTPRequest Wrapper](https://gitlab.com/AnimusDesign/KotlinJavaScriptWrappers) - A thin wrapper layer around javascript xml http request.