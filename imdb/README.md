# Summary 

This module sets up a sample database. A postgresql database that will be running
through docker. 

#### Docker

The `docker.sh` file is utilized to start the database. It defaults to the standard
port of `5432`. 

*Note if you wish to change the port, you will also have to update the `build.gradle` 
file.*

#### Source Data

[The source data is obtained from IMDB.](https://www.imdb.com/interfaces/#plain)

Download the files, extract and place in `src/main/resources`.

#### Populating the Database

Due to the size, I'm not including them in the repository.