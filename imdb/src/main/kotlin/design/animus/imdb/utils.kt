package design.animus.imdb

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import javax.sql.DataSource

/**
 * Helper method to load the Jooq DSL Context.
 *
 * Given a properties file name. Load the properties file.
 * Constructing a DriverManager for the expected SQL database.
 *
 * The expected input for a database connection are as follows.
 *
 * * db.url: The JDBC url for connecting.
 * * db.username: The database username to authenticate with.
 * * db.password The database password to authenticate with.
 *
 * @param properties_file The properties file containing database information to load.
 * @author Animus Null
 */
fun getDBContext(backoff: Int = 1, dbHost: String = "localhost", dbPort: Number = 5432): HikariDataSource {
    val config = HikariConfig()
    config.username = "imdb"
    config.jdbcUrl = "jdbc:postgresql://${dbHost}:${dbPort}/imdb"
    config.password = "imdb"
    config.maximumPoolSize = 65
    return HikariDataSource(config)
}

val IMDBContext = getDBContext()

inline fun <T : DataSource, R> T.connect(block: (V: DSLContext) -> R): R {
    val connection = this.connection
    val dsl = DSL.using(connection, SQLDialect.POSTGRES)
    try {
        return block(dsl)
    } finally {
        dsl.close()
        connection.close()
    }
}
