package design.animus.imdb

import java.io.File
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleCrew.TITLE_CREW as TitleCrewTable


typealias CreateMapFunction = (List<String>) -> Map<String, Any>


fun commitLines(lines: List<String>, createMap: CreateMapFunction) {
    val records = lines.map {
        it.split("\t").map { if (it.contains("\\N")) "" else it }
    }
    IMDBContext.connect { dsl ->
        dsl.batch(
                records.map { columns ->
                    dsl.insertInto(TitleCrewTable).set(createMap(columns)).onDuplicateKeyIgnore()
                }
        ).execute()

    }
}

fun insertBase(fileName: String, createMap: CreateMapFunction) {
    println("Reading and Inserting File $fileName")
    val reader = File(fileName).bufferedReader()
    var lines = mutableListOf<String>()
    var i = 0
    val columns = reader.readLine().split("\t")
    while (true) {
        var line: String? = reader.readLine()
        if (line == null && lines.isNotEmpty()) {
            commitLines(lines, createMap)
            break
        } else if (line == null && lines.isEmpty()) {
            break
        } else {
            line = line!!
            if (i < 100000) {
                lines.add(line)
            } else if (i > 100000) {
                commitLines(lines, createMap)
                lines.clear()
                i = 0
            }
            i += 1
        }
    }
}

fun main(args: Array<String>) {
    insertBase("./samples/title.crew.tsv") { columns ->
        mapOf(
                "tconst" to columns[0],
                "directors" to if (columns[1].isNotBlank()) columns[1].split(",") else listOf(),
                "writers" to if (columns[2].isNotBlank()) columns[1].split(",") else listOf()
        )
    }
}