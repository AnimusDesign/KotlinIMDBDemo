# Summary

This project is a sample project, demonstrating
a Kotlin backend. Providing both a REST and GraphQL
end point. It was done for the New York Brooklyn
Kotlin MeetUp.

* [Google Slides](https://docs.google.com/presentation/d/1RpLQuiblDlJzfA_sJxe6B_QjIYQqrl9I9eRJCxfzrPw/edit?usp=sharing)
* [Meetup Group](https://www.meetup.com/Brooklyn-Kotlin)

## Data Source

This is based on sample data from IMDB. [See this link.](https://www.imdb.com/interfaces/)
For the available data.

**Warning this requires a large SQL dump file.**

*The link is provided in the docker folder.*

## Modules / Directories

* *api* The backend layer.
    * Built with Jooby
    * REST / GraphQL endpoint.
    * Data retrieval helpers.
* *imdb* Data base bindings.
    * Built via [Jooq CodeGen](https://www.jooq.org/doc/3.11/manual/code-generation/).
    * A thin JDBC layer.
* *apollo-engine* The graphql api gateway.
    * [Utilizing apollo engine.](https://www.apollographql.com/engine) Provide a gateway.
    * You will need to register for an api key.
* *web* A react frontend.
    * Retrieves data via REST layer.
* *common* Data classes for the different sections.
    * *all* The platform agnostic base.
    * *jvm* Bindings for the API backend with GraphQL annotations.
    * *javascript* Bindings for the react frontend.
    * *Warning bug in Javascript, web frontend. Detailed in web.*

## Docker Items

* Graphite for metric collection of HTTP service.
* Grafana for viewing metrics.
* Postgres as the primary store.

## Deliverable

* REST endpoint providing access to IMDB data layer.
* GraphQL service providing access to IMDB data layer.
* Post metrics to graphite from the HTTP service.
* Consumption of REST endpoints from Kotlin+ReactJS

## Running

**API**

*Requires database connection*

```bash
./gradlew :api:run
```

**Web**

*See the web folder README, there was a bug I couldn't fix and a symlink is needed.*

```bash
./gradlew :web:run
```

**NGINX**

To prevent cross site origin request. A local proxy server will be needed.
Below is the configuration I used.

```bash
        location /apollo {
            proxy_pass  http://localhost:4000/apollo;
        }

        location /swagger {
            proxy_pass  http://localhost:8080/swagger;
        }

        location /api {
            proxy_pass  http://localhost:8080/api;
        }

        location / {
            proxy_pass  http://localhost:8088;
        }
```