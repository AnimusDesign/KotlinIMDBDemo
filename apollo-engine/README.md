# Summary

Script to launch apollo engine. Which will proxy
graphql requests. Much like a REST api gateway. As noted
you will need an api key. 

The purpose of this is to stitch together a number of graphql
endpoints together. This is much like a REST api gateway. It
will combine several graphql schemas to appear as one big
schema. Allowing development teams to work in a micro service
approach.

In addition it provides performance tracing, caching and other
features. *Note not all features are present in GraphQL Java.*

### Configuration

#### Specifying Backend

```javascript
  origins: [{
    http: {
      // The URL that the Proxy should use to connect to your
      // GraphQL server.
      url: 'http://127.0.0.1:8080/graphql',
    },
  }],
```

The origins specify what graphql backends to stitch together.
Here we are just providing one. Which is the api module in this
project.

#### Specifying Frontend

```javascript
  frontends: [{
    port: 4000,
    endpoints: ['/apollo/graphql',  "/graphiql"],
  }],
```

Here you specify what port the apollo engine listens on. Then what
endpoints it listens on. Anything else will be proxied through to
the backend graphql services.

### Launch

```bash
node engine.js
```

