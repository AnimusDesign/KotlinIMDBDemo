# Summary

This will spin up the additional docker instances for the project.

* Graphite a time series data store for metrics.
* Grafana a dashboard visualization tool.

Grafana will be accessible at:

> http://localhost:3000

You will want to add a graphite data source. 

* [Using graphite](http://docs.grafana.org/features/datasources/graphite/)
* Data Source Configuration
	* Host: *http://192.168.0.2*
	* Basic Auth: 
		* User guest
		* Password guest
