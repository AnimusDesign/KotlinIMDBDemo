package design.animus.kotlingraphqlrestpresentation.api

import com.codahale.metrics.graphite.Graphite
import com.codahale.metrics.graphite.GraphiteReporter
import com.codahale.metrics.jvm.FileDescriptorRatioGauge
import com.codahale.metrics.jvm.GarbageCollectorMetricSet
import com.codahale.metrics.jvm.MemoryUsageGaugeSet
import com.codahale.metrics.jvm.ThreadStatesGaugeSet
import design.animus.kotlingraphqlrestpresentation.api.controllers.GraphQLEndpoint
import design.animus.kotlingraphqlrestpresentation.api.controllers.v1.NameBasicv1
import design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title.TitleAKAv1
import design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title.TitleCrewv1
import design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title.TitleEpisodev1
import design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title.Titlev1
import design.animus.kotlingraphqlrestpresentation.api.controllers.v2.title.TitleAKAv2
import design.animus.kotlingraphqlrestpresentation.api.graphql.buildSchema
import graphql.GraphQL
import graphql.execution.instrumentation.tracing.TracingInstrumentation
import org.jooby.apitool.ApiTool
import org.jooby.json.Jackson
import org.jooby.metrics.Metrics
import java.util.concurrent.TimeUnit
import org.jooby.run as JoobyRun

// Constants relating to Jooby.
val Swagger = ApiTool()
val MetricInstance = Metrics()

//  Constants Relating to GraphQL
val SCHEMA = buildSchema()!!
var GRAPHQL = GraphQL.newGraphQL(SCHEMA)
        .instrumentation(TracingInstrumentation())
        .build()

fun main(args: Array<String>) {
    JoobyRun(*args) {
        use(Jackson()
                .raw()
        )
        use(Swagger
                .filter { r -> r.pattern().startsWith("/api") }
                .swagger()
                .raml())
        use(MetricInstance
                .request()
                .ping()
                .threadDump()
                .metric("memory", MemoryUsageGaugeSet())
                .metric("threads", ThreadStatesGaugeSet())
                .metric("gc", GarbageCollectorMetricSet())
                .metric("fs", FileDescriptorRatioGauge())
                .reporter { registry ->
                    val graphite = Graphite("localhost", 2003)
                    val reporter = GraphiteReporter.forRegistry(registry).build(graphite)
                    reporter.start(30, TimeUnit.SECONDS)
                    reporter
                }
        )
        use(Titlev1::class)
        use(TitleEpisodev1::class)
        use(TitleCrewv1::class)
        use(TitleAKAv1::class)
        use(TitleAKAv2::class)
        use(NameBasicv1::class)
        use(GraphQLEndpoint::class)
    }
}