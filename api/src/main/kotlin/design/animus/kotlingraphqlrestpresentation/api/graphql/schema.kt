package design.animus.kotlingraphqlrestpresentation.api.graphql

import design.animus.kotlingraphqlrestpresentation.api.graphql.service.NameBasicService
import design.animus.kotlingraphqlrestpresentation.api.graphql.service.TitleAKAService
import design.animus.kotlingraphqlrestpresentation.api.graphql.service.TitleBasicService
import graphql.schema.GraphQLSchema
import io.leangen.graphql.GraphQLSchemaGenerator
import org.slf4j.LoggerFactory


private val schemaLogger = LoggerFactory.getLogger("design.animus.kotlingraphqlrestpresentation.api.graphql")

fun buildSchema(): GraphQLSchema? {
    schemaLogger.debug("In build schema")
    val titleAKAService = TitleAKAService()
    val nameBasicService = NameBasicService()
    val titleBasicService = TitleBasicService()
    return GraphQLSchemaGenerator()
            .withBasePackages("design.animus.kotlingraphqlrestpresentation.api.graphql")
            .withOperationsFromSingleton(titleAKAService)
            .withOperationsFromSingleton(nameBasicService)
            .withOperationsFromSingleton(titleBasicService)
            .generate()
}
