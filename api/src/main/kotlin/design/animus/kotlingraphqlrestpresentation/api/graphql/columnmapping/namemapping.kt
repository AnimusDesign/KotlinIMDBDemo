package design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping
import design.animus.kotlingraphqlrestpresentation.imdb.tables.NameBasic.NAME_BASIC as NameBasicTable

val NameBasicTableFieldsMap = mapOf(
        "nconst" to NameBasicTable.NCONST,
        "primaryName" to NameBasicTable.PRIMARYNAME,
        "birthYear" to NameBasicTable.BIRTHYEAR,
        "deathYear" to NameBasicTable.DEATHYEAR,
        "primaryProfession" to NameBasicTable.PRIMARYPROFESSION,
        "knownForTitles" to NameBasicTable.KNOWNFORTITLES
)