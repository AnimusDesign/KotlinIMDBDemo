package design.animus.kotlingraphqlrestpresentation.api.graphql.service

import design.animus.common.records.imdb.title.TitleAKA
import design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping.TitleAKATableFieldsMap
import design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping.getFieldsFromGraphQLEnvironment
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getAllTitleAKABase
import graphql.language.Field
import io.leangen.graphql.annotations.GraphQLArgument
import io.leangen.graphql.annotations.GraphQLEnvironment
import io.leangen.graphql.annotations.GraphQLQuery
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka.TITLE_AKA as TitleAKATable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleAka as TitleAKAPOJO
import org.jooq.Field as JField


class TitleAKAService {

    @GraphQLQuery(name = "titleAKA")
    fun getAllTitleAKAByPage(@GraphQLArgument(name = "page") page: Int,
                             @GraphQLEnvironment env: List<Field>): List<TitleAKA> {
        val tableFields = getFieldsFromGraphQLEnvironment(TitleAKATableFieldsMap, env)
        return getAllTitleAKABase(page, tableFields).first
    }

}