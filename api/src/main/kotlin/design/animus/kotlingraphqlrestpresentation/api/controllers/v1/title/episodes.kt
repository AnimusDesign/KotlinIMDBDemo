package design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title

import design.animus.common.records.imdb.CommonResponse
import design.animus.common.records.imdb.title.TitleBasicResponse
import design.animus.common.records.imdb.title.TitleEpisodeResponse
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getEpisodesforTitleSeason
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getSeasonsForTitle
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getTitleById
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleBasicRecord
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleEpisodeRecord
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import javax.inject.Inject


@Path("/api/v1/title")
class TitleEpisodev1 @Inject constructor() {
    @GET
    @Path("/:titleId/seasons")
    fun getTitleSeasons(req: org.jooby.Request): CommonResponse<Int> {
        val titleId = req.param("titleId").value()
        val seasons = getSeasonsForTitle(titleId)
        return CommonResponse(
                error = false,
                page = 0,
                maxPage = 0,
                data = seasons
        )
    }

    @GET
    @Path("/:titleId/season/:season/episodes")
    fun getTitleEpisodes(req: org.jooby.Request): TitleEpisodeResponse {
        val titleId = req.param("titleId").value()
        val season = req.param("season").intValue()
        val episodes = getEpisodesforTitleSeason<TitleEpisodeRecord>(titleId, season)
        return TitleEpisodeResponse(
                error = false,
                page = 0,
                maxPage = 0,
                data = episodes
        )
    }

    @GET
    @Path("/:titleId/season/:season/episode/:episode")
    fun getTitleEpisode(req: org.jooby.Request): TitleBasicResponse {
        val episodeId = req.param("episode").value()
        val episode = getTitleById<TitleBasicRecord>(episodeId)
        return TitleBasicResponse(
                error = false,
                page = 0,
                maxPage = 0,
                data = listOf(episode)
        )
    }

}