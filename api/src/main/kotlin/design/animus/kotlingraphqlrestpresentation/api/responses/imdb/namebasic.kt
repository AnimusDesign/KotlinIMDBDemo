package design.animus.kotlingraphqlrestpresentation.api.responses.imdb


import design.animus.common.records.imdb.name.NameBasic
import design.animus.common.records.imdb.name.NameBasicResponse
import design.animus.kotlingraphqlrestpresentation.api.responses.NameBasicVersions

fun buildNameBasicResponse(inData: List<NameBasic>, page: Int, maxPage: Int,
                          version: NameBasicVersions = NameBasicVersions.LATEST): NameBasicResponse {
    return when (version) {
        NameBasicVersions.LATEST, NameBasicVersions.ONE -> {
            NameBasicResponse(
                    error = false,
                    page = page,
                    maxPage = maxPage,
                    data = inData
            )
        }
    }
}