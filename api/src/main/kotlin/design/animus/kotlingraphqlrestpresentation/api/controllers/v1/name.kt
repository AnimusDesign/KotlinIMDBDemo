package design.animus.kotlingraphqlrestpresentation.api.controllers.v1


import design.animus.common.records.imdb.name.NameBasicResponse
import design.animus.kotlingraphqlrestpresentation.api.responses.NameBasicVersions
import design.animus.kotlingraphqlrestpresentation.api.responses.imdb.buildNameBasicResponse
import design.animus.kotlingraphqlrestpresentation.api.utils.name.getAllNameBasic
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.NameBasicRecord
import org.jooby.Request
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import javax.inject.Inject
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka.TITLE_AKA as TitleAKATable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleAka as TitleAKAPojo


@Path("/api/v1/name")
class NameBasicv1 @Inject constructor() {
    val version = NameBasicVersions.ONE

    @GET
    fun getAllNameBasicFirstPage(): NameBasicResponse {
        val (records, maxPage) = getAllNameBasic<NameBasicRecord>(1)
        return buildNameBasicResponse(records, 1, maxPage, version)
    }

    @GET
    @Path("/:page")
    fun getAllNameBasicWithPage(req: Request): NameBasicResponse {
        val startTime = System.nanoTime()
        val page = req.param("page").intValue()
        val (records, maxPage) = getAllNameBasic<NameBasicRecord>(page)
        val endTime = System.nanoTime()
        println("Method took ${(endTime - startTime) / 1000000} milliseconds to complete.")
        return buildNameBasicResponse(records, page, maxPage, version)
    }
}