package design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title

import design.animus.common.records.imdb.CommonResponse
import design.animus.common.records.imdb.title.TitleBasicResponse
import design.animus.kotlingraphqlrestpresentation.api.responses.TitleBasicVersions
import design.animus.kotlingraphqlrestpresentation.api.responses.imdb.buildTitleBasicResponse
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getAvailableTitleTypes
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getTitleByTitleType
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleBasicRecord
import org.jooby.Request
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import javax.inject.Inject
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleBasic.TITLE_BASIC as TitleBasicTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleBasic as TitleBasicPOJO

@Path("/api/v1/title/")
class Titlev1 @Inject constructor() {
    val version = TitleBasicVersions.ONE

    @GET
    @Path("/types")
    fun getTitleTypes(req: Request): CommonResponse<String> {
        println("In get Title Types")
        val titleTypes = getAvailableTitleTypes()
        println("Retrieved types of $titleTypes")
        return CommonResponse(
                page = 1,
                maxPage = 0,
                error = false,
                data = titleTypes
        )
    }

    @GET
    @Path("/type/:type/:page")
    fun getByType(req: Request): TitleBasicResponse {
        val page = req.param("page").value().toInt()
        val titleType = req.param("type").value()
        val (titles, maxPage) = getTitleByTitleType<TitleBasicRecord>(titleType, page)
        return buildTitleBasicResponse(titles, page, maxPage, version)
    }
}