package design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title


import design.animus.common.records.imdb.title.TitleAKABase
import design.animus.common.records.imdb.title.TitleAKAResponse
import design.animus.kotlingraphqlrestpresentation.api.responses.TitleAKAVersions
import design.animus.kotlingraphqlrestpresentation.api.responses.imdb.buildTitleAKAResponse
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getAllTitleAKABase
import org.jooby.Request
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import javax.inject.Inject
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka.TITLE_AKA as TitleAKATable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleAka as TitleAKAPojo

val TitleAKAv1TableFields = arrayOf(
        TitleAKATable.TITLEID,
        TitleAKATable.ORDERING,
        TitleAKATable.TITLE,
        TitleAKATable.REGION,
        TitleAKATable.LANGUAGE,
        TitleAKATable.TYPES
).toMutableList()

@Path("/api/v1/title/")
class TitleAKAv1 @Inject constructor() {
    val version = TitleAKAVersions.ONE


    @GET
    fun getAllTitleAKA(): TitleAKAResponse<TitleAKABase> {
        val (records, maxPage) = getAllTitleAKABase(1, TitleAKAv1TableFields)
        return buildTitleAKAResponse(records, 1, maxPage, version)
    }

    @GET
    @Path("/:page")
    fun getAllTitleAKAWithPage(req: Request): TitleAKAResponse<TitleAKABase> {
        val page = req.param("page").intValue()
        val (records, maxPage) = getAllTitleAKABase(page, TitleAKAv1TableFields)
        return buildTitleAKAResponse(records, 1, maxPage, version)
    }
}