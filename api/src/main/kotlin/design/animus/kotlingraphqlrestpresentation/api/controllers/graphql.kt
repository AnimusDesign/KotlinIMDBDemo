package design.animus.kotlingraphqlrestpresentation.api.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import design.animus.kotlingraphqlrestpresentation.api.GRAPHQL
import graphql.ExecutionInput
import graphql.GraphQLError
import org.jooby.MediaType
import org.jooby.Request
import org.jooby.Response
import org.jooby.mvc.GET
import org.jooby.mvc.POST
import org.jooby.mvc.Path
import org.slf4j.LoggerFactory
import javax.inject.Inject

val endpointLogger = LoggerFactory.getLogger("design.animus.kotlingraphqlrestpresentation.api.controllers")!!

data class GraphQLResponse(
        val data: Any,
        val extensions: Any?
)

data class GraphQLRequest(val query: String = "",
                          val operationName: String = "",
                          val variables: Map<String, Any> = mapOf()
)

data class GraphQLErrorResponse(
        val errors: List<GraphQLError>
)

/**
 * * [Upstream link.](https://graphql.org/learn/serving-over-http/)
 *
 * See the above link for information on the expected HTTP endpoints for GraphQL.
 * In addition it outlines the object models.
 */
@Path("/graphql")
class GraphQLEndpoint @Inject constructor() {
    private val mapper = ObjectMapper()

    private fun executeQuery(graphQLQuery: GraphQLRequest, httpRSP: Response): String {
        endpointLogger.debug("Sending query to graphql $graphQLQuery")


        val queryInput = ExecutionInput.newExecutionInput()
                .query(graphQLQuery.query)
                .context(mutableMapOf<String, Any>()) //the relevant line
                .build()
        val rawRSP = GRAPHQL.execute(queryInput)
        return when (rawRSP.errors.isEmpty()) {
            true -> {
                endpointLogger.info("Query executed successfully.")
                val rsp = GraphQLResponse(
                        data = rawRSP.getData(),
                        extensions = rawRSP.extensions
                )
                httpRSP.status(200)
                mapper.writeValueAsString(rsp)
            }
            false -> {
                endpointLogger.info("Query failed to execute.")
                httpRSP.status(203)
                mapper.writeValueAsString(GraphQLErrorResponse(errors = rawRSP.errors))
            }
        }
    }

    @GET
    fun getGraphQL(req: Request, httpRSP: Response): String? {
        httpRSP.type(MediaType.json)
        val query = req.param("query").value() as String?
        return if (query == null) {
            mapper.writeValueAsString(mapOf<String, Any>("error" to true, "message" to "No query specified."))
        } else {
            val graphQLQuery = mapper.readValue(query, GraphQLRequest::class.java)
            executeQuery(graphQLQuery, httpRSP)
        }

    }

    @POST
    fun postQuery(req: Request, httpRSP: Response): String {
        val startTime = System.nanoTime()
        println("Making request")
        httpRSP.type(MediaType.json)
        val query = req.body().value()
        endpointLogger.debug("Received a query of $query")
        val graphQLQuery = mapper.readValue(query, GraphQLRequest::class.java)
        val endTime = System.nanoTime()
        println("Method took ${(endTime - startTime) / 1000000} milliseconds to complete.")
        return executeQuery(graphQLQuery, httpRSP)
    }
}