package design.animus.kotlingraphqlrestpresentation.api.utils.title

import design.animus.common.records.imdb.title.TitleEpisode
import design.animus.common.records.imdb.title.TitleID
import design.animus.imdb.IMDBContext
import design.animus.imdb.connect
import design.animus.kotlingraphqlrestpresentation.api.TableFields
import org.jooq.Record
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleBasic.TITLE_BASIC as TitleBasicTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleEpisode.TITLE_EPISODE as TitleEpisodeTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleBasic as TitleBasicPOJO
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleEpisode as TitleEpisodePOJO


fun castJooqEpisodeToCommon(record: TitleEpisodePOJO) = TitleEpisode(
        titleId = record.tconst ?: "",
        parentTitleId = record.parenttconst ?: "",
        seasonNumber = record.seasonnumber ?: 0,
        episodeNumber = record.episodenumber ?: 0
)

fun getSeasonsForTitle(titleId: TitleID) = IMDBContext.connect {
    it
            .select(TitleEpisodeTable.SEASONNUMBER)
            .distinctOn(TitleEpisodeTable.SEASONNUMBER)
            .from(TitleEpisodeTable)
            .where(TitleEpisodeTable.PARENTTCONST.eq(titleId))
            .orderBy(TitleEpisodeTable.SEASONNUMBER.asc())
            .fetch()
            .into(TitleEpisodePOJO::class.java)
            .map { it.seasonnumber }
}

fun <R: Record> getDetailedEpisodesForASeason(titleId: TitleID, season: Int,
                                  tableFields: TableFields<R>? = null) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(TitleBasicTable) else it.selectFrom(TitleBasicTable)
    query
            .where(TitleBasicTable.TCONST.`in`(it.select(TitleEpisodeTable.TCONST)
                    .from(TitleEpisodeTable)
                    .where(TitleEpisodeTable.PARENTTCONST.eq(titleId))
                    .and(TitleEpisodeTable.SEASONNUMBER.eq(season))
                    .orderBy(TitleEpisodeTable.EPISODENUMBER.asc())
            ))
    println(query)
            query
            .fetch()
            .into(TitleBasicPOJO::class.java)
            .map { castJooqRecordtoCommonTitleBasic(it) }

}

fun <R : Record> getEpisodesforTitleSeason(titleId: TitleID, season: Int,
                                           tableFields: TableFields<R>? = null) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(TitleEpisodeTable) else it.selectFrom(TitleEpisodeTable)

    query
            .where(TitleEpisodeTable.PARENTTCONST.eq(titleId))
            .and(TitleEpisodeTable.SEASONNUMBER.eq(season))
            .orderBy(TitleEpisodeTable.EPISODENUMBER.asc())
            .fetch()
            .into(TitleEpisodePOJO::class.java)
            .map { castJooqEpisodeToCommon(it) }
}
