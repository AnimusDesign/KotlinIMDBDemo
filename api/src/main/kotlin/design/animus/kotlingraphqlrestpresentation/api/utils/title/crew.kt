package design.animus.kotlingraphqlrestpresentation.api.utils.title

import design.animus.kotlingraphqlrestpresentation.api.TableFields
import design.animus.common.records.imdb.name.Name
import design.animus.common.records.imdb.title.TitleCrew
import design.animus.common.records.imdb.title.TitleID
import design.animus.imdb.IMDBContext
import design.animus.imdb.connect
import org.jooq.Record
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleCrew.TITLE_CREW as TitleCrewTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleCrew as TitleCrewPOJO

fun castJooqRecordToTitleCrew(record: TitleCrewPOJO) = TitleCrew(
        titleId = record.tconst ?: "",
        directors = record.directors?.filter { it.isNotBlank() }?.map { Name(it) } ?: listOf(),
        writers = record.writers?.filter { it.isNotBlank() }?.map { Name(it) } ?: listOf()
)

fun <R : Record> getCrewForTitle(titleID: TitleID, tableFields: TableFields<R>? = null) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(TitleCrewTable) else it.selectFrom(TitleCrewTable)
        query.where(TitleCrewTable.TCONST.eq(titleID))
    println(query)
    castJooqRecordToTitleCrew(
            query.fetchOne().into(TitleCrewPOJO::class.java)
    )
}