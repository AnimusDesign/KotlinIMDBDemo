package design.animus.kotlingraphqlrestpresentation.api.utils.title

import design.animus.common.records.imdb.title.TitleBasic
import design.animus.common.records.imdb.title.TitleID
import design.animus.imdb.IMDBContext
import design.animus.imdb.connect
import design.animus.kotlingraphqlrestpresentation.api.TableFields
import org.jooq.Record
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleBasic.TITLE_BASIC as TitleBasicTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleBasic as TitleBasicPOJO


fun castJooqRecordtoCommonTitleBasic(record: TitleBasicPOJO) = TitleBasic(
        titleId = record.tconst ?: "",
        titleType = record.titletype ?: "",
        primaryTitle = record.primarytitle ?: "",
        originalTitle = record.originaltitle ?: "",
        isAdult = record.isadult ?: false,
        startYear = record?.startyear?.toIntOrNull() ?: 0,
        endYear = record?.endyear?.toIntOrNull() ?: 0,
        runTimeMinutes = record.runtimeminutes ?: 0,
        genres = record.genres?.toList() ?: listOf()
)


fun getAvailableTitleTypes() = IMDBContext.connect {
    it.selectDistinct(TitleBasicTable.TITLETYPE)
            .from(TitleBasicTable)
            .fetch()
}.map { it.get(0, String::class.java) }.toList()

fun <R : Record> getTitleByTitleType(type: String,
                                     page: Int, tableFields: TableFields<R>? = null,
                                     limit: Int = 25) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(TitleBasicTable) else it.selectFrom(TitleBasicTable)
    val titles = query
            .where(TitleBasicTable.TITLETYPE.likeIgnoreCase(type))
            .orderBy(TitleBasicTable.TITLETYPE.asc())
            .offset(if (page > 1) page * limit else 0)
            .limit(limit)
            .fetchInto(TitleBasicPOJO::class.java)
            .map { castJooqRecordtoCommonTitleBasic(it) }
    val maxPage = it.select(TitleBasicTable.TCONST)
            .from(TitleBasicTable)
            .where(TitleBasicTable.TITLETYPE.likeIgnoreCase(type))
            .fetchCount()
    Pair(titles, maxPage / limit)
}

fun <R : Record> getTitleById(titleId: TitleID, tableFields: TableFields<R>? = null) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(TitleBasicTable) else it.selectFrom(TitleBasicTable)
    query
            .where(TitleBasicTable.TCONST.eq(titleId))
    println(query)
    query
            .fetchOne()
            .into(TitleBasicPOJO::class.java)
            .let { castJooqRecordtoCommonTitleBasic(it) }
}

fun <R : Record> getTitleByIDs(titleIDs: List<TitleID>, tableFields: TableFields<R>? = null) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(TitleBasicTable) else it.selectFrom(TitleBasicTable)
    query
            .where(TitleBasicTable.TCONST.`in`(titleIDs))
    println(query)
    query
            .fetch()
            .into(TitleBasicPOJO::class.java)
            .map { castJooqRecordtoCommonTitleBasic(it) }
}

fun <R : Record> getAllTitles(page: Int, tableFields: TableFields<R>? = null, limit: Int = 25) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(TitleBasicTable) else it.selectFrom(TitleBasicTable)
            .orderBy(TitleBasicTable.PRIMARYTITLE.asc())
            .offset(if (page > 1) page * limit else 0)
            .limit(limit)
    println(query.query)
    query.fetchInto(TitleBasicPOJO::class.java)
}.map {
    castJooqRecordtoCommonTitleBasic(it)
}
