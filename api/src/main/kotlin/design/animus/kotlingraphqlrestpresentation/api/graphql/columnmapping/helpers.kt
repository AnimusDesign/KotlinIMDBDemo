package design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping

import graphql.language.Field
import org.jooq.Record
import org.jooq.TableField
import java.io.Serializable


fun <R : Record> getTableField(fieldString: String, inMap: Map<String, TableField<R, out Serializable>>) = inMap.getOrDefault(
        fieldString, null
)

fun <R: Record> getFieldsFromGraphQLEnvironment(inMap: Map<String, TableField<R, out Serializable>>,
                                                env: List<Field>) = env.first().selectionSet.selections.mapNotNull {
    val field = it as? Field
    if (field != null) getTableField(field.name, inMap) else null
}.toMutableList()