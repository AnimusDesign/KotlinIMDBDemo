package design.animus.kotlingraphqlrestpresentation.api

import org.jooq.TableField
import java.io.Serializable

typealias TableFields<R> = MutableList<TableField<R, out Serializable>>
