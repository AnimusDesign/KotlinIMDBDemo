package design.animus.kotlingraphqlrestpresentation.api.utils.title

import design.animus.common.records.imdb.title.TitleAKA
import design.animus.imdb.IMDBContext
import design.animus.imdb.connect
import design.animus.kotlingraphqlrestpresentation.api.TableFields
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka
import org.jooq.Record
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka.TITLE_AKA as TitleAKATable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleAka as TitleAKAPOJO

fun castJooqRecordtoTitleAKA(record: TitleAKAPOJO) = TitleAKA(
        titleid = record.titleid,
        title = record.title ?: "",
        region = record.region ?: "",
        ordering = record.ordering ?: 0,
        language = record.language ?: "",
        types = record.types?.toList() ?: listOf(),
        attributes = record.attributes?.toList() ?: listOf(),
        isOriginalTitle = if (record.isoriginaltitle == null) false else record.isoriginaltitle
)

fun getMaxPageForTitleAKA(): Int = IMDBContext.connect {
    it.selectCount().from(TitleAka.TITLE_AKA).fetchOne()
}.get(0, Int::class.java)

fun <R : Record> getAllTitleAKABase(page: Int, fields: TableFields<R>): Pair<List<TitleAKA>, Int> {
    val limit = 50
    val records = IMDBContext.connect {
        val query = it.select(fields)
                .from(TitleAKATable)
                .orderBy(TitleAKATable.TITLE.asc())
                .offset(if (page > 1) page * limit else 0)
                .limit(limit)
        println(query.query)
        query.fetchInto(TitleAKAPOJO::class.java)
    }.map {castJooqRecordtoTitleAKA(it) }
    return Pair(records, getMaxPageForTitleAKA())
}