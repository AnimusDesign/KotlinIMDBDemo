package design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title


import design.animus.common.records.imdb.title.TitleCrewResponse
import design.animus.kotlingraphqlrestpresentation.api.responses.TitleBasicVersions
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getCrewForTitle
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleCrewRecord
import org.jooby.Request
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import javax.inject.Inject
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleBasic.TITLE_BASIC as TitleBasicTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleBasic as TitleBasicPOJO

@Path("/api/v1/title/crew")
class TitleCrewv1 @Inject constructor() {
    val version = TitleBasicVersions.ONE

    @GET
    @Path("/:titleId")
    fun getCrewForTitleById(req: Request): TitleCrewResponse {
        val titleId = req.param("titleId").value()
        val crew = getCrewForTitle<TitleCrewRecord>(titleId)
        return TitleCrewResponse(
                error = false,
                page = 0,
                maxPage = 0,
                data = listOf(crew)
        )
    }

}