package design.animus.kotlingraphqlrestpresentation.api.controllers.v2.title


import design.animus.common.records.imdb.title.TitleAKABase
import design.animus.common.records.imdb.title.TitleAKAResponse
import design.animus.common.records.imdb.title.TitleAKAv2Response
import design.animus.kotlingraphqlrestpresentation.api.controllers.v1.title.TitleAKAv1TableFields
import design.animus.kotlingraphqlrestpresentation.api.responses.TitleAKAVersions
import design.animus.kotlingraphqlrestpresentation.api.responses.imdb.buildTitleAKAResponse
import design.animus.kotlingraphqlrestpresentation.api.utils.title.getAllTitleAKABase
import org.jooby.Request
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import javax.inject.Inject
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka.TITLE_AKA as TitleAKATable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleAka as TitleAKAPojo

val TitleAKAv2TableFields = (TitleAKAv1TableFields +
        arrayOf(TitleAKATable.ATTRIBUTES, TitleAKATable.ISORIGINALTITLE)).toMutableList()


@Path("/api/v2/title")
class TitleAKAv2 @Inject constructor() {
    val version = TitleAKAVersions.TWO

    @GET
    fun getAllTitleAKA(): TitleAKAv2Response {
        val (records, maxPage) = getAllTitleAKABase(1, TitleAKAv2TableFields)
        return buildTitleAKAResponse(records, 1, maxPage, version) as TitleAKAv2Response
    }

    @GET
    @Path("/:page")
    fun getAllTitleAKAWithPage(req: Request): TitleAKAResponse<TitleAKABase> {
        val page = req.param("page").intValue()
        val (records, maxPage) = getAllTitleAKABase(page, TitleAKAv2TableFields)
        return buildTitleAKAResponse(records, 1, maxPage, version)
    }
}