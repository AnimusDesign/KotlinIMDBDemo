package design.animus.kotlingraphqlrestpresentation.api.responses.imdb

import design.animus.common.records.imdb.ResponseBase
import design.animus.common.records.imdb.title.*
import design.animus.kotlingraphqlrestpresentation.api.responses.TitleAKAVersions
import design.animus.kotlingraphqlrestpresentation.api.responses.TitleBasicVersions

fun buildTitleAKAResponse(inData: List<TitleAKA>, page: Int, maxPage: Int,
                          version: TitleAKAVersions = TitleAKAVersions.LATEST): TitleAKAResponse<TitleAKABase> {
    return when (version) {
        TitleAKAVersions.ONE -> {
            TitleAKAResponse(
                    error = false,
                    page = page,
                    maxPage = maxPage,
                    data = inData.map {
                        TitleAKAv1(it.titleid, it.ordering, it.title, it.region, it.language, it.types)
                    }

            )
        }
        TitleAKAVersions.TWO, TitleAKAVersions.LATEST -> {
            TitleAKAResponse(
                    error = false,
                    page = page,
                    maxPage = maxPage,
                    data = inData.map {
                        TitleAKAv2(it.titleid, it.ordering, it.title, it.region, it.language, it.types, it.attributes, it.isOriginalTitle)
                    }
            )
        }
    }
}

fun buildTitleBasicResponse(inData: List<TitleBasic>, page: Int, maxPage: Int,
                            version: TitleBasicVersions = TitleBasicVersions.LATEST): TitleBasicResponse {
    return TitleBasicResponse(
            error = false,
            page = page,
            maxPage = maxPage,
            data = inData
    )
}