package design.animus.kotlingraphqlrestpresentation.api.responses

enum class TitleAKAVersions(version: String) {
    ONE("one"),
    TWO("two"),
    LATEST("latest")
}

enum class NameBasicVersions(version: String) {
    ONE("one"),
    LATEST("latest")
}

enum class TitleBasicVersions(version: String) {
    ONE("one"),
    LATEST("latest")
}