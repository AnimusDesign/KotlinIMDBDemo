package design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping

import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleEpisodeRecord
import org.jooq.TableField
import java.io.Serializable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka.TITLE_AKA as TitleAKATable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleBasic.TITLE_BASIC as TitleBasicTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleCrew.TITLE_CREW as TitleCrewTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleEpisode.TITLE_EPISODE as TitleEpisodeTable


val TitleAKATableFieldsMap = mapOf(
        "titleId" to TitleAka.TITLE_AKA.TITLEID,
        "title" to TitleAka.TITLE_AKA.TITLE,
        "region" to TitleAka.TITLE_AKA.REGION,
        "ordering" to TitleAka.TITLE_AKA.ORDERING,
        "language" to TitleAka.TITLE_AKA.LANGUAGE,
        "types" to TitleAka.TITLE_AKA.TYPES,
        "attributes" to TitleAka.TITLE_AKA.ATTRIBUTES,
        "originalTitle" to TitleAka.TITLE_AKA.ISORIGINALTITLE
)

val TitleBasicTableFieldsMap = mapOf(
        "titleId" to TitleBasicTable.TCONST,
        "titleType" to TitleBasicTable.TITLETYPE,
        "primaryTitle" to TitleBasicTable.PRIMARYTITLE,
        "originalTitle" to TitleBasicTable.ORIGINALTITLE,
        "isAdult" to TitleBasicTable.ISADULT,
        "startYear" to TitleBasicTable.STARTYEAR,
        "endYear" to TitleBasicTable.ENDYEAR,
        "runTimeMinutes" to TitleBasicTable.RUNTIMEMINUTES,
        "genres" to TitleBasicTable.GENRES
)

val TitleCrewTableFieldsMap = mapOf(
        "titleId" to TitleCrewTable.TCONST,
        "directors" to TitleCrewTable.DIRECTORS,
        "writers" to TitleCrewTable.WRITERS
)

val TitleEpisodeTableFieldsMap = mapOf<String, TableField<TitleEpisodeRecord, out Serializable>>(
        "titleId" to TitleEpisodeTable.TCONST,
        "parentTitleId" to TitleEpisodeTable.PARENTTCONST,
        "seasonNumber" to TitleEpisodeTable.SEASONNUMBER,
        "episodeNumber" to TitleEpisodeTable.EPISODENUMBER
)

