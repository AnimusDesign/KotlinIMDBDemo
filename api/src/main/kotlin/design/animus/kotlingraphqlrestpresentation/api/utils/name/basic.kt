package design.animus.kotlingraphqlrestpresentation.api.utils.name

import design.animus.common.records.imdb.name.Name
import design.animus.common.records.imdb.name.NameBasic
import design.animus.common.records.imdb.title.TitleAKA
import design.animus.common.records.imdb.title.TitleSummary
import design.animus.imdb.IMDBContext
import design.animus.imdb.connect
import design.animus.kotlingraphqlrestpresentation.api.TableFields
import design.animus.kotlingraphqlrestpresentation.imdb.tables.TitleAka
import org.jooq.Record
import design.animus.kotlingraphqlrestpresentation.imdb.tables.NameBasic.NAME_BASIC as NameBasicTable
import design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.NameBasic as NameBasicPOJO

fun castJooqRecordToNameBasic(record: NameBasicPOJO) = NameBasic(
        nconst = record.nconst ?: "",
        primaryName = record.primaryname ?: "",
        birthYear = if (record.birthyear != null && record.birthyear.isNotBlank()) record.birthyear.toInt() else 0 ?: 0,
        deathYear = if (record.deathyear != null && record.deathyear.isNotBlank()) record.deathyear.toInt() else 0 ?: 0,
        primaryProfession = record.primaryprofession?.toList() ?: listOf(),
        knownForTitles = record.knownfortitles?.map { TitleSummary(titleID = it) } ?: listOf()
)

fun <R : Record> getByNameID(nameId: String, tableFields: TableFields<R>? = null) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(NameBasicTable) else it.selectFrom(NameBasicTable)
    query
            .where(NameBasicTable.NCONST.eq(nameId))
    println(query)
    query
            .fetchOne()
            .into(NameBasicPOJO::class.java)
            .let { castJooqRecordToNameBasic(it) }
}

fun <R : Record> getByNamesIDs(nameIds: List<Name>, tableFields: TableFields<R>? = null) = IMDBContext.connect {
    val query = if (tableFields != null) it.select(tableFields).from(NameBasicTable) else it.selectFrom(NameBasicTable)
    query
            .where(NameBasicTable.NCONST.`in`(nameIds.filter { it.nameId.isNotBlank() }.map { it.nameId }))
    println(query)
    query
            .fetch()
            .into(NameBasicPOJO::class.java)
            .map { castJooqRecordToNameBasic(it) }
}


fun <R : Record> getAllNameBasic(page: Int = 1, tableFields: TableFields<R>? = null, limit: Int = 25): Pair<List<NameBasic>, Int> {
    val startNameQeryTime = System.nanoTime()
    val records = IMDBContext.connect {
        val actualQueryStartTime = System.nanoTime()
        val query = if (tableFields != null) it.select(tableFields).from(NameBasicTable) else it.selectFrom(NameBasicTable)
        query
                .offset(if (page > 1) page * limit else 0)
                .limit(limit)
        println(query)
        val rsp = query

                .fetchInto(NameBasicPOJO::class.java)
        val actualQueryEndTime = System.nanoTime()
        println("The query took ${ (actualQueryEndTime - actualQueryStartTime) / 1000000 } milliseconds to complete.")
        rsp

    }.map { castJooqRecordToNameBasic(it) }
    val endNameQeryTime = System.nanoTime()
    println("Max Page query took ${(endNameQeryTime - startNameQeryTime) / 1000000} milliseconds to complete.")
    return Pair(records, 0)
}

fun <R : Record> getTitleByID(titleID: String, tableFields: TableFields<R>? = null): TitleAKA? {
    val record = IMDBContext.connect {
        val rsp = it.selectFrom(TitleAka.TITLE_AKA)
                .where(TitleAka.TITLE_AKA.TITLEID.eq(titleID))
                .fetchOne()
        val x = if (rsp != null)
            rsp.into(design.animus.kotlingraphqlrestpresentation.imdb.tables.pojos.TitleAka::class.java)
        else {
            println("Failed to find title of $titleID")
            null
        }
        x
    }
    return if (record != null) TitleAKA(titleid = record.titleid, title = record.title, attributes = record.attributes.toList(),
            isOriginalTitle = record.isoriginaltitle, ordering = record.ordering, region = record.region,
            language = record.language, types = record.types.toList())
    else null
}