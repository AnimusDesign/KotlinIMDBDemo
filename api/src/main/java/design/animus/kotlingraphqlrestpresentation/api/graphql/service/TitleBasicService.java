package design.animus.kotlingraphqlrestpresentation.api.graphql.service;

import design.animus.common.records.imdb.name.NameBasic;
import design.animus.common.records.imdb.title.*;
import design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping.NamemappingKt;
import design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping.TitlemappingKt;
import design.animus.kotlingraphqlrestpresentation.api.utils.title.BasicKt;
import design.animus.kotlingraphqlrestpresentation.api.utils.title.CrewKt;
import design.animus.kotlingraphqlrestpresentation.api.utils.title.EpisodeKt;
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.NameBasicRecord;
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleBasicRecord;
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleEpisodeRecord;
import graphql.language.Field;
import io.leangen.graphql.annotations.*;
import org.jooq.TableField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping.HelpersKt.getFieldsFromGraphQLEnvironment;
import static design.animus.kotlingraphqlrestpresentation.api.utils.name.BasicKt.getByNamesIDs;
import static design.animus.kotlingraphqlrestpresentation.api.utils.title.BasicKt.getTitleById;
import static design.animus.kotlingraphqlrestpresentation.api.utils.title.EpisodeKt.getEpisodesforTitleSeason;

public class TitleBasicService {
    @GraphQLQuery
    public TitleCrew crew(@GraphQLContext TitleBasic title) {
        return CrewKt.getCrewForTitle(title.getTitleId(), null);
    }

    @GraphQLQuery
    public List<NameBasic> directorDetails(@GraphQLContext TitleCrew crew,
                                           @GraphQLEnvironment List<Field> env) {
         List<TableField<NameBasicRecord, ? extends Serializable>> tableFields = getFieldsFromGraphQLEnvironment(
                NamemappingKt.getNameBasicTableFieldsMap(), env);
        return getByNamesIDs(crew.getDirectors(),
                tableFields);
    }

    @GraphQLQuery
    public List<NameBasic> writerDetails(@GraphQLContext TitleCrew crew,
                                         @GraphQLEnvironment List<Field> env) {
        List<TableField<NameBasicRecord, ? extends Serializable>> tableFields = getFieldsFromGraphQLEnvironment(
                NamemappingKt.getNameBasicTableFieldsMap(), env);
        return getByNamesIDs(crew.getWriters(),
                tableFields);
    }

    @GraphQLQuery
    public List<Season> seasons(@GraphQLContext TitleBasic title) {
        return EpisodeKt.getSeasonsForTitle(title.getTitleId())
                .stream().map(
                        (Season::new)
                )
                .collect(Collectors.toList());
    }

    @GraphQLQuery
    public List<TitleEpisode> episodes(@GraphQLContext Season season,
                                       @GraphQLRootContext("parentTitleId") String parentTitleId,
                                       @GraphQLEnvironment List<Field> env) {
        List<TableField<TitleEpisodeRecord, ? extends Serializable>> tableFields = getFieldsFromGraphQLEnvironment(
                TitlemappingKt.getTitleEpisodeTableFieldsMap(), env);
        return getEpisodesforTitleSeason(parentTitleId, season.getSeasonNumber(), tableFields);

    }

    @GraphQLQuery
    public List<TitleBasic> episodeDetails(@GraphQLContext Season season,
                                           @GraphQLRootContext("parentTitleId") String parentTitleId,
                                           @GraphQLEnvironment List<Field> env) {
        List<TableField<TitleBasicRecord, ? extends Serializable>> episodeTableFields = getFieldsFromGraphQLEnvironment(
                TitlemappingKt.getTitleBasicTableFieldsMap(), env);
        return EpisodeKt.getDetailedEpisodesForASeason(parentTitleId, season.getSeasonNumber(), episodeTableFields);
    }

    @GraphQLQuery(name = "title", description = "Base query for information on titles")
    public List<TitleBasic> getTitles(@GraphQLArgument(name = "page") int page,
                                      @GraphQLEnvironment List<Field> env) {
        List<TableField<TitleBasicRecord, ? extends Serializable>> tableFields = getFieldsFromGraphQLEnvironment(
                TitlemappingKt.getTitleBasicTableFieldsMap(), env);
        return design.animus.kotlingraphqlrestpresentation.api.utils.title.BasicKt.getAllTitles(page, tableFields, 25);
    }

    @GraphQLQuery(name = "titleById", description = "Base query for information on titles")
    public TitleBasic getTitles(@GraphQLArgument(name = "id") String titleId,
                                @GraphQLRootContext Map<String, Object> global,
                                @GraphQLEnvironment List<Field> env) {
        global.put("parentTitleId", titleId);
        List<TableField<TitleBasicRecord, ? extends Serializable>> tableFields = getFieldsFromGraphQLEnvironment(
                TitlemappingKt.getTitleBasicTableFieldsMap(), env);
        return getTitleById(titleId, tableFields);
    }
}
