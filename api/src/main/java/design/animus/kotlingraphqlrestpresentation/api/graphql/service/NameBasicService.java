package design.animus.kotlingraphqlrestpresentation.api.graphql.service;

import design.animus.common.records.imdb.name.NameBasic;
import design.animus.common.records.imdb.title.TitleAKA;
import design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping.TitlemappingKt;
import design.animus.kotlingraphqlrestpresentation.api.responses.NameBasicVersions;
import design.animus.kotlingraphqlrestpresentation.api.utils.name.BasicKt;
import design.animus.kotlingraphqlrestpresentation.imdb.tables.records.TitleBasicRecord;
import graphql.language.Field;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLContext;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLQuery;
import kotlin.Pair;
import org.jooq.TableField;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static design.animus.kotlingraphqlrestpresentation.api.graphql.columnmapping.HelpersKt.getFieldsFromGraphQLEnvironment;
import static design.animus.kotlingraphqlrestpresentation.api.utils.name.BasicKt.getTitleByID;

public class NameBasicService {
    private NameBasicVersions version;
    private Integer limit;

    public NameBasicService() {
        this.version = NameBasicVersions.LATEST;
        this.limit = 25;
    }

    @GraphQLQuery
    public List<TitleAKA> relatedTitles(@GraphQLContext NameBasic name,
                                        @GraphQLEnvironment List<Field> env) {
        List<TableField<TitleBasicRecord, ? extends Serializable>> tableFields = getFieldsFromGraphQLEnvironment(
                TitlemappingKt.getTitleBasicTableFieldsMap(), env);
        return name.getKnownForTitles().stream().map(
                (titleID -> getTitleByID(titleID.getTitleID(), tableFields))
        ).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @GraphQLQuery(name = "nameBasic")
    public List<NameBasic> getNameBasicBase(@GraphQLArgument(name = "page") Integer page) {
        Pair<List<NameBasic>, Integer> rsp = BasicKt.getAllNameBasic(page, null, 25);
        return rsp.getFirst();
    }
}
