# Summary

This module provides HTTP endpoints, for both REST
and GraphQL. In addition to basic monitoring support.
The end points will pull data from a PostgreSQL data store.
Which contains sample data from IMDB.

The backend is built using [Jooby](https://jooby.org/). You can 
use whichever http routing framework you feel comfortable with.

We will be utilizing the API documentation module. Which provides
Swagger and RAML generation. Metrics, via drop wizard metrics.
Which reports to graphite, and is displayed via graphite.

#### Why not JDK10

Initially I developed this with JDK 10. But the graphite
metric reporter was using an unsafe `com.sun.management` library.
Which as of JDK 9+ were blocked. So we can make the GraphQL service
classes slightly less verbose, but at the cost of some metrics.