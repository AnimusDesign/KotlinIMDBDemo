# Summary

This module, and submodules. Contain data classes relevant
to all portions of the application.

* **common** Is platform agnostic, and the base implementation.
* **jvm** Is for the JVM/backend, and adds things like GraphQL support.
* **js** Is for Javascript and the react frontend.

The classes are used for the response structure of both
the REST and GraphQL API. Which is then consumed by the react
frontend.

## Name Space Structure

The name space is meant to mimic the IMDB sample data structure.
Which also closely mimics the database layer as well. Title
has sub tables like *aka, episode, crew, basic*. While name
simply has basic.
