package design.animus.common.records.imdb.title

import design.animus.common.records.imdb.ResponseBase
import kotlinx.serialization.Serializable

abstract class TitleEpisodeBase {
    abstract val titleId: TitleID
}

@Serializable
expect class TitleEpisode : TitleEpisodeBase {
    override val titleId: TitleID
    val parentTitleId: TitleID
    val seasonNumber: Int
    val episodeNumber: Int
}

data class TitleEpisodeResponse(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<TitleEpisode>
) : ResponseBase<TitleEpisode>()