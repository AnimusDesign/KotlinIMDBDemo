package design.animus.common.records.imdb.name

import design.animus.common.records.imdb.ResponseBase
import design.animus.common.records.imdb.title.TitleSummary
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer

typealias NameID = String

data class Name(
        val nameId: NameID
)

abstract class NameBasicBase {
    abstract val nconst: String
}

expect class NameBasic : NameBasicBase {
    override val nconst: String
    val primaryName: String
    val birthYear: Int
    val deathYear: Int
    val primaryProfession: List<String>
    val knownForTitles: List<TitleSummary>
}

@Serializable
data class NameBasicResponse(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<NameBasic>
) : ResponseBase<NameBasic>()
