package design.animus.common.records.imdb.title

import design.animus.common.records.imdb.ResponseBase
import kotlinx.serialization.Serializable

typealias TitleID = String

expect class TitleSummary {
    val titleID : TitleID
}

abstract class TitleAKABase {
    abstract val titleid: TitleID
}

@Serializable
expect class TitleAKA : TitleAKABase {
    override val titleid: TitleID
    val attributes: List<String>
    val isOriginalTitle: Boolean
    val ordering: Int
    val title: String
    val region: String
    val language: String
    val types: List<String>
}

data class TitleAKAResponse<T>(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<T>
) : ResponseBase<T>()