package design.animus.common.records.imdb.title

import design.animus.common.records.imdb.ResponseBase
import kotlinx.serialization.Serializable


data class TitleAKAv1(
        override val titleid: String,
        val ordering: Int,
        val title: String,
        val region: String,
        val language: String,
        val types: List<String>
) : TitleAKABase()

@Serializable
data class TitleAKAv1Response(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<TitleAKAv1>
) : ResponseBase<TitleAKAv1>()