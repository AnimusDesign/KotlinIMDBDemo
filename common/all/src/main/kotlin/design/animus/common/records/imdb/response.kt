package design.animus.common.records.imdb

import design.animus.common.records.imdb.title.TitleBasic


abstract class ResponseBase<D> {
    abstract val error: Boolean
    abstract val page: Int
    abstract val maxPage: Int
    abstract val data: List<D>
}

data class CommonResponse<D>(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<D>
) : ResponseBase<D>()