package design.animus.common.records.imdb.title

import design.animus.common.records.imdb.ResponseBase
import kotlinx.serialization.Serializable


data class TitleAKAv2(
        override val titleid: String,
        val ordering: Int,
        val title: String,
        val region: String,
        val language: String,
        val types: List<String>,
        val attributes: List<String>,
        val isOriginalTitle: Boolean
) : TitleAKABase()


@Serializable
data class TitleAKAv2Response(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<TitleAKAv2>
) : ResponseBase<TitleAKAv2>()