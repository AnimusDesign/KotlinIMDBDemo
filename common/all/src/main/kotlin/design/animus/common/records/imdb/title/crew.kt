package design.animus.common.records.imdb.title


import design.animus.common.records.imdb.ResponseBase
import design.animus.common.records.imdb.name.Name
import kotlinx.serialization.Serializable

abstract class TitleCrewBase {
    abstract val titleId: TitleID
}

@Serializable
expect class TitleCrew : TitleCrewBase {
    override val titleId: TitleID
    val directors: List<Name>
    val writers : List<Name>
}

data class TitleCrewResponse(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<TitleCrewBase>
) : ResponseBase<TitleCrewBase>()