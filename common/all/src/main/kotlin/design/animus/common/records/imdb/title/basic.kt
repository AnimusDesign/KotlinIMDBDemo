package design.animus.common.records.imdb.title

import design.animus.common.records.imdb.ResponseBase
import kotlinx.serialization.Serializable

abstract class TitleBase {
    abstract val titleId: TitleID
}

@Serializable
expect class TitleBasic : TitleBase {
    override val titleId: TitleID
    val titleType: String
    val primaryTitle: String
    val originalTitle: String
    val isAdult: Boolean
    val startYear: Int
    val endYear: Int
    val runTimeMinutes: Int
    val genres: List<String>
}

data class TitleBasicResponse(
        override val error: Boolean,
        override val page: Int,
        override val maxPage: Int,
        override val data: List<TitleBasic>
) : ResponseBase<TitleBasic>()