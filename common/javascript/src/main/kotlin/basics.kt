package design.animus.common.records.imdb.name

import design.animus.common.records.imdb.title.TitleSummary
import kotlinx.serialization.Serializable

@Serializable
actual class NameBasic(actual override val nconst: String,
                       actual val primaryName: String,
                       actual val birthYear: Int,
                       actual val deathYear: Int,
                       actual val primaryProfession: List<String>,
                       actual val knownForTitles: List<TitleSummary>
) : NameBasicBase()

@Serializable
data class NameBasicResponseJS(
        val error: Boolean,
        val page: Int,
        val maxPage: Int,
        val data: List<NameBasic>
)