package design.animus.common.records.imdb.title

import kotlinx.serialization.Serializable

@Serializable
actual class TitleBasic(
        actual override val titleId: TitleID,
        actual val titleType: String,
        actual val primaryTitle: String,
        actual val originalTitle: String,
        actual val isAdult: Boolean,
        actual val startYear: Int,
        actual val endYear: Int,
        actual val runTimeMinutes: Int,
        actual val genres: List<String>
) : TitleBase()