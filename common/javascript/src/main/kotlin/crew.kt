package design.animus.common.records.imdb.title

import design.animus.common.records.imdb.name.Name
import kotlinx.serialization.Serializable

@Serializable
actual class TitleCrew(
        actual override val titleId: TitleID,
        actual val directors: List<Name>,
        actual val writers: List<Name>
) : TitleCrewBase()