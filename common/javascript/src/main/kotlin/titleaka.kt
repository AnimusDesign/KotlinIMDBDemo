package design.animus.common.records.imdb.title

import kotlinx.serialization.Serializable

@Serializable
actual class TitleSummary(
    actual val titleID : TitleID
)


@Serializable
actual class TitleAKA(
        actual override val titleid: TitleID,
        actual val attributes: List<String>,
        actual val isOriginalTitle: Boolean,
        actual val ordering: Int,
        actual val title: String,
        actual val region: String,
        actual val language: String,
        actual val types: List<String>
) : TitleAKABase()