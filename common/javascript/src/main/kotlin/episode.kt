package design.animus.common.records.imdb.title

import kotlinx.serialization.Serializable

@Serializable
actual class TitleEpisode(
        actual override val titleId: TitleID,
        actual val parentTitleId: TitleID,
        actual val seasonNumber: Int,
        actual val episodeNumber: Int
) : TitleEpisodeBase()