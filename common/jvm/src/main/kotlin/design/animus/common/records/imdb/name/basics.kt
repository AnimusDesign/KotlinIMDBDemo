package design.animus.common.records.imdb.name

import design.animus.common.records.imdb.title.TitleSummary
import io.leangen.graphql.annotations.GraphQLQuery


actual class NameBasic(@GraphQLQuery(name = "nconst") actual override val nconst: String,
                       @GraphQLQuery(name = "primaryName") actual val primaryName: String,
                       @GraphQLQuery(name = "birthYear") actual val birthYear: Int,
                       @GraphQLQuery(name = "deathYear") actual val deathYear: Int,
                       @GraphQLQuery(name = "primaryProfession") actual val primaryProfession: List<String>,
                       @GraphQLQuery(name = "knownForTitles") actual val knownForTitles: List<TitleSummary>
) : NameBasicBase()