package design.animus.common.records.imdb.title

import design.animus.common.records.imdb.name.Name
import io.leangen.graphql.annotations.GraphQLQuery
import kotlinx.serialization.Serializable

@Serializable
actual class TitleCrew(
        @GraphQLQuery(name = "titleId") actual override val titleId: TitleID,
        @GraphQLQuery(name = "directors", description = "A list of directors for the tile. (Has Supporting queries)")
        actual val directors: List<Name>,
        @GraphQLQuery(name = "writers", description = "A list of writers for the tile. (Has Supporting queries)")
        actual val writers: List<Name>
) : TitleCrewBase()