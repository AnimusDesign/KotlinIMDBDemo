package design.animus.common.records.imdb.title

import io.leangen.graphql.annotations.GraphQLQuery
import kotlinx.serialization.Serializable

@Serializable
actual class TitleBasic(
    @GraphQLQuery(name = "titleId") actual override  val titleId: TitleID,
    @GraphQLQuery(name = "titleType") actual val titleType: String,
    @GraphQLQuery(name = "primaryTitle") actual val primaryTitle: String,
    @GraphQLQuery(name = "originalTitle") actual val originalTitle: String,
    @GraphQLQuery(name = "isAdult") actual val isAdult: Boolean,
    @GraphQLQuery(name = "startYear") actual val startYear: Int,
    @GraphQLQuery(name = "endYear") actual val endYear: Int,
    @GraphQLQuery(name = "runTimeMinutes") actual val runTimeMinutes: Int,
    @GraphQLQuery(name = "genres") actual val genres: List<String>
) : TitleBase()