package design.animus.common.records.imdb.title

import io.leangen.graphql.annotations.GraphQLQuery
import kotlinx.serialization.Serializable

@Serializable
data class Season(
        val seasonNumber: Int
)

@Serializable
data class Episode(
        val episodeId: TitleID
)

@Serializable
actual class TitleEpisode(
        @GraphQLQuery(name = "titleId", description = "The id of the episode.") actual override val titleId: TitleID,
        @GraphQLQuery(name = "parentTitleId", description = "The parent show this episode is from.") actual val parentTitleId: TitleID,
        @GraphQLQuery(name = "seasonNumber") actual val seasonNumber: Int,
        @GraphQLQuery(name = "episodeNumber") actual val episodeNumber: Int
) : TitleEpisodeBase()