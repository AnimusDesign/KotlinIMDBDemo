package design.animus.common.records.imdb.title

import kotlinx.serialization.Serializable
import io.leangen.graphql.annotations.GraphQLQuery
@Serializable
actual class TitleSummary (
    @GraphQLQuery(name = "titleID") actual val titleID : TitleID
)


@Serializable
actual class TitleAKA(
        @GraphQLQuery(name = "titleid") actual override val titleid: TitleID,
        @GraphQLQuery(name = "attributes") actual val attributes: List<String>,
        @GraphQLQuery(name = "isOriginalTitle") actual val isOriginalTitle: Boolean,
        @GraphQLQuery(name = "ordering") actual val ordering: Int,
        @GraphQLQuery(name = "title") actual val title: String,
        @GraphQLQuery(name = "region") actual val region: String,
        @GraphQLQuery(name = "language") actual val language: String,
        @GraphQLQuery(name = "types") actual val types: List<String>
) : TitleAKABase()